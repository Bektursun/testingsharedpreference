package com.bektursun.sharedpreferencetesting

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        Fresco.initialize(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sharedPreference: SharedPreference = SharedPreference(this)
        btnSave.setOnClickListener {

            val name = editName.text.toString()
            val email = editEmail.text.toString()

            if (name != "" || email != "") {

                sharedPreference.save("name", name)
                sharedPreference.save("email", email)
                Toast.makeText(this@MainActivity, "Data Stored", Toast.LENGTH_SHORT).show()

            } else {
                Toast.makeText(this, "Enter value should have something data", Toast.LENGTH_SHORT).show()
            }

            // save int
            sharedPreference.save("first", 1)

            // save boolean
            sharedPreference.save("bool", true)

            Toast.makeText(this@MainActivity, "Saved to shared pref", Toast.LENGTH_SHORT).show()
            editName.text.clear()
            editEmail.text.clear()

        }

        btnAddImage.setOnClickListener {

            if (editImageUrl.text.toString() != "") {
                val imageUrl = editImageUrl.text.toString()
                loadFrescoImage(imageUrl)
                Toast.makeText(this, "Image is loaded", Toast.LENGTH_SHORT).show()
            }
            else {
                Toast.makeText(this, "Enter url", Toast.LENGTH_SHORT).show()
            }

        }

        btnRetrieve.setOnClickListener {

            if (sharedPreference.getValueString("name") != null) {
                editName.hint = sharedPreference.getValueString("name")
                searchNameResultTextView.text = sharedPreference.getValueString("name")
                Toast.makeText(this@MainActivity, "Data retrieved", Toast.LENGTH_SHORT).show()
                editName.text.clear()
                editEmail.text.clear()

                }
            else {
                searchNameResultTextView.text = getString(R.string.search_results) + " No value found"
                searchEmailResultTextView.text = getString(R.string.search_email) + " No value found"
            }

            if (sharedPreference.getValueString("email") != null) {
                editEmail.hint = sharedPreference.getValueString("email")
                searchEmailResultTextView.text = sharedPreference.getValueString("email")
                Toast.makeText(this@MainActivity, "Data retrieved", Toast.LENGTH_SHORT).show()

            } else {

                searchNameResultTextView.text = getString(R.string.search_results) + " No value found"
                searchEmailResultTextView.text = getString(R.string.search_email) + " No value found"
            }
        }

        btnClear.setOnClickListener {
            sharedPreference.clearSharedPreference()
            Toast.makeText(this@MainActivity, "Data Cleared", Toast.LENGTH_SHORT).show()
            editName.hint = ""
            editEmail.hint = ""
            searchNameResultTextView.text = getString(R.string.search_results)
            searchEmailResultTextView.text = getString(R.string.search_email)
        }
        loadWithGlide.setOnClickListener {
            val intent = Intent(this, ImageActivity::class.java)
            startActivity(intent)
        }
    }

    private fun loadFrescoImage(url: String) {
        // val uri: Uri = Uri.parse("https://raw.githubusercontent.com/facebook/fresco/master/docs/static/logo.png")
        val image: SimpleDraweeView = findViewById(R.id.myImageView)
        image.setImageURI(Uri.parse(url), null)
    }
}
