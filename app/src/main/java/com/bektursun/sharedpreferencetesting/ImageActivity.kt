package com.bektursun.sharedpreferencetesting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_image.*

class ImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        loadImageBtn.setOnClickListener {
            val url = imageUrlEditText.text.toString()
            if (url != "") {
                loadImageWithGlide(it, url)
                Toast.makeText(this, "Image loaded.", Toast.LENGTH_SHORT).show()
                imageUrlEditText.text.clear()
            } else {
                Toast.makeText(this, "Enter image url", Toast.LENGTH_SHORT).show()
            }
        }

        loadImageWithPicassoBtn.setOnClickListener {
            val intent = Intent(this, LoadImageWithPicassoActivity::class.java)
            startActivity(intent)
        }
    }

    private fun loadImageWithGlide(view: View, imageUrl: String) {

        Glide.with(view)
            .load(imageUrl)
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_background)
            .into(imageImageView)
    }
}
