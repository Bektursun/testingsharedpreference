package com.bektursun.sharedpreferencetesting

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_load_image_with_picasso.*

class LoadImageWithPicassoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_load_image_with_picasso)

        picassoLoadImageBtn.setOnClickListener {
            val imageUrl = imageUrl.text.toString()
            loadImageWithPicasso(imageUrl)
        }
    }

    private fun loadImageWithPicasso(url: String) {

        if (url != "") {
            Picasso.get()
                .load(url)
                .into(picassoImageView)
            Toast.makeText(this, "Image loading", Toast.LENGTH_SHORT).show()
            imageUrl.text.clear()
        }
        else {
            Toast.makeText(this, "Enter image url", Toast.LENGTH_SHORT).show()
        }
    }
}
